import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomePageComponent } from "./components/home-page/home-page.component";
import { CategoriesComponent } from "./components/categories/categories.component";
import { CategoryEditingComponent } from "./components/categories/category-editing/category-editing.component";
import { NotFoundComponent } from "./shared/not-found/not-found.component";
import { AuthGuard } from "./auth/auth.guard";
import { RegistrationComponent } from "./components/registration/registration.component";

const routes: Routes = [
  { path: '', component: HomePageComponent },
  { path: 'categories', component: CategoriesComponent, canActivate: [AuthGuard] },
  { path: 'categories/:id', component: CategoryEditingComponent, canActivate: [AuthGuard] },
  { path: 'not-found', component: NotFoundComponent },
  { path: 'registration', component: RegistrationComponent },
  { path: '**', redirectTo: 'not-found' }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      initialNavigation: 'enabled'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
