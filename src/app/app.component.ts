import { Component, ComponentFactoryResolver, OnInit, ViewChild } from '@angular/core';
import { trigger, style, state, transition, animate } from "@angular/animations";
import { Router } from "@angular/router";

import { TranslateService } from "@ngx-translate/core";
import { Store } from "@ngrx/store";

import { LoginComponent } from "./shared/modals/login/login.component";
import { RefDirective } from "./shared/ref-directive/ref.directive";
import { ConfirmModalComponent } from "./shared/modals/confirm-modal/confirm-modal.component";
import { AuthService } from "./auth/auth.service";
import { closeLoginModal, closeLogoutModal } from "./stores/modalStore/modalStore.actions";
import { isLoginModal, isLogoutModal } from "./stores/modalStore/modalStore.selectors";
import { Registration } from "./auth/auth.service.api";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  animations: [
    trigger('visibleAnimation', [
      state('invisible', style({
        opacity: 0
      })),
      state('visible', style({
        opacity: 1
      })),
      transition('invisible => visible', animate('1s linear'))
    ])
  ]
})
export class AppComponent implements OnInit {

  public title: string = 'mentoring-project';
  public visibleState: string = 'invisible';

  @ViewChild(RefDirective)
  private refDirective: RefDirective = <RefDirective>{};

  constructor(
    private store$: Store,
    private router: Router,
    private authService: AuthService,
    private factoryResolver: ComponentFactoryResolver,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.checkCurrentUser();

    setTimeout(() => this.visibleState = 'visible', 0)
    this.translateService.use('ru');

    this.subscribeToLoginModal();
    this.subscribeToLogoutModal();
  }

  checkCurrentUser(): void {
    const isCurrentUser = localStorage.getItem('currentUser');
    if (isCurrentUser) {
      const user: Registration = JSON.parse(isCurrentUser);
      // timeout to get IndexedDB connection initialize
      setTimeout(() => {
        this.authService.login(user.userName, user.password)
          .catch(() => {
            this.authService.logout();
            alert(this.translateService.instant('unsuccessfulReAuthError'));
          })
      }, 1000);
    }
  }

  subscribeToLoginModal(): void {
    this.store$.select(isLoginModal).subscribe(value => {
      if (value) {
        this.login();
      }
    });
  }

  subscribeToLogoutModal(): void {
    this.store$.select(isLogoutModal).subscribe(value => {
      if (value) {
        this.logout();
      }
    });
  }

  logout(): void {
    const factoryConfirmModal = this.factoryResolver.resolveComponentFactory(ConfirmModalComponent);
    this.refDirective.viewContainerRef.clear();

    const component = this.refDirective.viewContainerRef.createComponent(factoryConfirmModal);

    component.instance.title = this.translateService.instant('logout');
    component.instance.confirmationMessage = this.translateService.instant('confirmLogout');
    component.instance.confirmHandler = (() => {
      this.authService.logout();
      this.refDirective.viewContainerRef.clear();
      this.store$.dispatch(closeLogoutModal());
      this.router.navigate(['']);
    });
    component.instance.onClose.subscribe(() => {
      this.refDirective.viewContainerRef.clear();
      this.store$.dispatch(closeLogoutModal());
    });
  }

  login(): void {
    const factoryLoginModal = this.factoryResolver.resolveComponentFactory(LoginComponent);
    this.refDirective.viewContainerRef.clear();

    const component = this.refDirective.viewContainerRef.createComponent(factoryLoginModal);

    component.instance.onClose.subscribe(() => {
      this.refDirective.viewContainerRef.clear();
      this.store$.dispatch(closeLoginModal());
    })
  }

}
