import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { TranslateModule, TranslateLoader, MissingTranslationHandler } from '@ngx-translate/core';
import { StoreModule } from "@ngrx/store";

import { AppRoutingModule } from './app-routing.module';
import { HeaderModule } from "./components/header/header.module";
import { SharedModule } from "./shared/shared.module";
import { CategoriesModule } from './components/categories/categories.module';
import { HomePageModule } from "./components/home-page/home-page.module";
import { AppComponent } from './app.component';
import { CustomTranslateLoader, MissingTranslationService } from '../locale/locale.api';
import { modalReducer } from "./stores/modalStore/modalStore.reducer";
import { currentUserReducer } from "./stores/userStore/user.reducer";
import { RegistrationModule } from "./components/registration/registration.module";

@NgModule({
  declarations: [
    AppComponent,
  ],
  imports: [
    TranslateModule.forRoot({
      loader: { provide: TranslateLoader, useClass: CustomTranslateLoader },
      missingTranslationHandler: { provide: MissingTranslationHandler, useClass: MissingTranslationService },
      useDefaultLang: false
    }),
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    FormsModule,
    StoreModule.forRoot({
      modal: modalReducer,
      currentUser: currentUserReducer
    }),
    ReactiveFormsModule,
    BrowserAnimationsModule,
    SharedModule,
    AppRoutingModule,
    HeaderModule,
    HomePageModule,
    CategoriesModule,
    RegistrationModule,
  ],
  providers: [],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
