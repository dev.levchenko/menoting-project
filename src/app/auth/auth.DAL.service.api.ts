import { Registration } from "./auth.service.api";

export interface IAuthDALService {

  saveUser(user: Registration): boolean;

  getUser(userName: string, password: string): Registration;

  updateUser(user: Registration): Registration;

  deleteUser(userName: string): boolean;

}
