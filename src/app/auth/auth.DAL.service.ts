import { Injectable } from "@angular/core";

import { IDBPDatabase } from 'idb';

import { Registration } from "./auth.service.api";
import { IndexedDBService } from "../services/indexedDB/indexedDB.service";

// Должен будет потом имплементироваться от интерфейса IAuthDALService
@Injectable({
  providedIn: 'root'
})
export class AuthDALService {

  private db = <IDBPDatabase>{};

  constructor(
    private indexedDBService: IndexedDBService
  ) {
    this.indexedDBService.getDBObject().subscribe(db => this.db = db);
  }

  async saveUser(user: Registration): Promise<void> {
    const transaction = this.db.transaction("users", "readwrite");
    const users = transaction.objectStore("users");

    await users.add(user);
  }

  async getUserByUserName(userName: string): Promise<Registration> {
    const transaction = this.db.transaction("users", "readonly");
    const users = transaction.objectStore("users");
    const userNameIndex = users.index('userName_index');

    return await userNameIndex.get(userName);
  }

  async getUserByEmail(email: string): Promise<Registration> {
    const transaction = this.db.transaction("users", "readonly");
    const users = transaction.objectStore("users");
    const emailIndex = users.index('email_index');

    return await emailIndex.get(email);
  }

}
