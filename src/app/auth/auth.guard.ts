import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';

import { Store } from "@ngrx/store";

import { AuthService } from "./auth.service";
import { showLoginModal } from "../stores/modalStore/modalStore.actions";

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  private isAuth: boolean = false;

  constructor(
    private store$: Store,
    private router: Router,
    private authService: AuthService
  ) { }

  canActivate(
    router: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Observable<boolean> | Promise<boolean> | boolean {

    this.authService.isAuthenticated().subscribe(isAuth => this.isAuth = isAuth);
    if (this.isAuth) {
      return true
    }

    this.store$.dispatch(showLoginModal());

    return false;
  }

}
