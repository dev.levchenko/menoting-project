import { Observable } from "rxjs";

export interface Registration {

  id: string;
  userName: string;
  email: string;
  phoneNumber?: string;
  birthDate?: string;
  password: string;
  confirmSendingEmails?: boolean;

}

export interface IAuthService {

  login(userName: string, password: string): Promise<boolean>;

  logout(): void;

  registration(user: Registration): Promise<boolean>;

  isAuthenticated(): Observable<boolean>;

}
