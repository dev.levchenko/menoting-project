import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { take } from "rxjs/operators";

import { Store } from "@ngrx/store";

import { IAuthService, Registration } from "./auth.service.api";
import { clearCurrentUser, setCurrentUser } from "../stores/userStore/user.actions";
import { AuthDALService } from "./auth.DAL.service";
import { getPasswordHash } from "../utils/getPasswordHash";

@Injectable({
  providedIn: 'root'
})
export class AuthService implements IAuthService {

  private isAuth = new BehaviorSubject<boolean>(false);

  constructor(
    private authDALService: AuthDALService,
    private store$: Store
  ) {}

  async login(userName: string, passwordHash: string): Promise<boolean> {
    const user = await this.authDALService.getUserByUserName(userName);

    if (!user || user.password !== passwordHash) {
      throw 'incorrectAuthDataError';
    }

    this.store$.dispatch(setCurrentUser(user));
    localStorage.setItem('currentUser', JSON.stringify(user));
    this.isAuth.next(true)

    return true;
  }

  logout(): void {
    this.isAuth.next(false)
    this.store$.dispatch(clearCurrentUser());
    localStorage.removeItem('currentUser');
  }

  async registration(user: Registration): Promise<boolean> {
    const isUserNameBusy = await this.authDALService.getUserByUserName(user.userName);
    if (isUserNameBusy) {
      throw 'userNameIsBusyError';
    }

    const isEmailBusy = await this.authDALService.getUserByEmail(user.email);
    if (isEmailBusy) {
      throw 'isEmailBusyError';
    }

    user.password = getPasswordHash(user.password);

    try {
      await this.authDALService.saveUser(user);
    } catch (error) {
      throw 'someRegistrationError';
    }

    this.isAuth.next(true)

    this.store$.dispatch(setCurrentUser(user));
    localStorage.setItem('currentUser', JSON.stringify(user));

    return true;
  }

  isAuthenticated(): Observable<boolean> {
    return this.isAuth.pipe(take(1));
  }

}
