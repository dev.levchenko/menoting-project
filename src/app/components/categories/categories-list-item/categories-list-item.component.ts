import { Component, ComponentFactoryResolver, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { Router } from "@angular/router";

import { TranslateService } from "@ngx-translate/core";

import { Category } from "../../../services/categories/categories.service.api";
import { ConfirmModalComponent } from "../../../shared/modals/confirm-modal/confirm-modal.component";
import { RefDirective } from "../../../shared/ref-directive/ref.directive";

@Component({
  selector: 'app-categories-item',
  templateUrl: './categories-list-item.component.html',
  styleUrls: ['./categories-list-item.component.scss']
})
export class CategoriesListItemComponent implements OnInit {

  @Output() private onDeleteCategory: EventEmitter<string> = new EventEmitter<string>();
  @Input() public category: Category = <Category>{};

  @ViewChild(RefDirective)
  refDirective: RefDirective = <RefDirective>{};

  public isClicked: boolean = false;

  constructor(
    private factoryResolver: ComponentFactoryResolver,
    private translateService: TranslateService,
    private router: Router
  ) {}

  ngOnInit(): void {}

  clickHandler(): void {
    this.isClicked = !this.isClicked;
  }

  deleteCategory(): void {
    const factoryConfirmModal = this.factoryResolver.resolveComponentFactory(ConfirmModalComponent);
    this.refDirective.viewContainerRef.clear();

    const component = this.refDirective.viewContainerRef.createComponent(factoryConfirmModal);

    component.instance.title = this.translateService.instant('deletingCategory');
    component.instance.confirmationMessage = this.translateService.instant('confirmCategoryDeleting');
    component.instance.confirmHandler = (() => this.onDeleteCategory.emit(this.category.id));
    component.instance.onClose.subscribe(() => {
      this.refDirective.viewContainerRef.clear();
    });
  }

  changeCategory(): void {
    this.router.navigate(['/categories', this.category.id]);
  }

}
