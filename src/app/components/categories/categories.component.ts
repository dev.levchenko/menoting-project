import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidationErrors, Validators } from "@angular/forms";
import { Observable } from "rxjs";

import { v4 as uuidv4 } from "uuid";

import { Category } from "../../services/categories/categories.service.api";
import { CategoriesService } from "../../services/categories/categories.service";

@Component({
  selector: 'app-categories',
  templateUrl: './categories.component.html',
  styleUrls: ['./categories.component.scss']
})
export class CategoriesComponent implements OnInit {

  public categories: Observable<Category[]> | undefined;

  public form: FormGroup = <FormGroup>{};

  constructor(
    private categoriesService: CategoriesService
  ) { }

  ngOnInit(): void {
    this.getCategories();
    this.initializeFormGroup();
  }

  initializeFormGroup(): void {
    this.form = new FormGroup({
      name: new FormControl('', [
        Validators.maxLength(60),
        Validators.required
      ]),
      description: new FormControl(''),
    });
  }

  getNameFormErrors(): ValidationErrors | null | undefined {
    return this.form.get('name')?.errors;
  }

  getNameFormMaxLengthError() {
    return this.getNameFormErrors()?.maxlength;
  }

  async getCategories(): Promise<void> {
    this.categories = await this.categoriesService.getCategories();
  }

  async saveCategory(): Promise<void> {
    const category: Category = {
      id: uuidv4(),
      name: this.form.value.name,
      howManyQuestions: 0,
      description: this.form.value.description,
      questions: []
    }

    await this.categoriesService.saveCategory(category);
  }

  async deleteCategory(categoryId: string): Promise<void> {
    await this.categoriesService.deleteCategory(categoryId);
  }

  identify(index: number, item: Category): string {
    return item.id;
  }

}
