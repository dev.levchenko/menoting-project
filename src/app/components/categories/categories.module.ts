import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { TranslateModule } from "@ngx-translate/core";

import { CategoriesComponent } from "./categories.component";
import { CategoriesListItemComponent } from "./categories-list-item/categories-list-item.component";
import { CategoryEditingComponent } from "./category-editing/category-editing.component";
import { SharedModule } from "../../shared/shared.module";

@NgModule({
  declarations: [
    CategoriesComponent,
    CategoriesListItemComponent,
    CategoryEditingComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule
  ],
  exports: [
    CategoriesComponent
  ]
})
export class CategoriesModule { }
