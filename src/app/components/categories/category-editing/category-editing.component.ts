import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from "@angular/router";
import { FormControl, FormGroup, ValidationErrors, Validators } from "@angular/forms";
import { Subscription } from "rxjs";

import { CategoriesService } from "../../../services/categories/categories.service";
import { Category } from "../../../services/categories/categories.service.api";
import { CustomValidators } from "../../../utils/customValidators";

@Component({
  selector: 'app-category-card',
  templateUrl: './category-editing.component.html',
  styleUrls: ['./category-editing.component.scss']
})
export class CategoryEditingComponent implements OnInit, OnDestroy {

  public category: Category | undefined;
  private categorySubscription: Subscription | undefined;

  public saveChangesError: string = '';

  public isQuestionVisible: boolean = false;
  public form: FormGroup = <FormGroup>{};

  constructor(
    private categoriesService: CategoriesService,
    private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.getCategory();
    this.initializeFormGroup();
  }

  ngOnDestroy(): void {
    this.categorySubscription?.unsubscribe();
  }

  getCategory(): void {
    const categoryId = this.activatedRoute.snapshot.params.id;
    this.categorySubscription = this.categoriesService.getCategoryById(categoryId)
      .subscribe(category => this.category = category);
  }

  initializeFormGroup(): void {
    this.form = new FormGroup({
      name: new FormControl(this.category?.name, [
        CustomValidators.categoryName,
        Validators.required
      ]),
      description: new FormControl(this.category?.description)
    });
  }

  getNameFormErrors(): ValidationErrors | null | undefined {
    return this.form.get('name')?.errors;
  }

  getNameFormMaxLengthError() {
    return this.getNameFormErrors()?.maxlength;
  }

  async saveChanges(): Promise<void> {
    if (!this.category) return;

    try {
      await this.categoriesService.updateCategory(this.category.id, {
        name: this.form.get('name')?.dirty ? this.form.value.name : '',
        description: this.form.get('description')?.dirty ? this.form.value.description : '',
      })
    } catch (error) {
      this.saveChangesError = error;
    }
  }

  showQuestions(): void {
    this.isQuestionVisible = !this.isQuestionVisible;
  }

  isSaveButtonDisabled(): boolean {
    return this.form.invalid
      || this.form.value.name === this.category?.name && this.form.value.description === this.category?.description
  }

  getValidationErrors(formControlName: string): string[] {
    const formControl = this.form.get(formControlName);
    if (formControl?.errors) {
      return Object.keys(formControl.errors as object);
    }
    return [];
  }
}
