import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { Observable } from "rxjs";

import { Store } from "@ngrx/store";

import { AuthService } from "../../../auth/auth.service";
import { showLoginModal, showLogoutModal } from "../../../stores/modalStore/modalStore.actions";
import { getCurrentUser } from "../../../stores/userStore/user.selectors";
import { Registration } from "../../../auth/auth.service.api";

@Component({
  selector: 'app-header-right-side',
  templateUrl: './header-right-side.component.html',
  styleUrls: ['./header-right-side.component.scss']
})
export class HeaderRightSideComponent implements OnInit {

  public currentUser$ = {} as Observable<Registration>;

  constructor(
    private router: Router,
    private store$: Store,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.getCurrentUser();
  }

  getCurrentUser(): void {
    this.currentUser$ = this.store$.select(getCurrentUser);
  }

  isAuthenticated(): Observable<boolean> {
    return this.authService.isAuthenticated()
  }

  login(): void {
    this.store$.dispatch(showLoginModal());
  }

  logout(): void {
    this.store$.dispatch(showLogoutModal());
  }

  registration(): void {
    this.router.navigate(['registration']);
  }

}
