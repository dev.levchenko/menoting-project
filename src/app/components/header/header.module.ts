import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from "@ngx-translate/core";

import { SharedModule } from "../../shared/shared.module";

import { HeaderComponent } from "./header.component";
import { HeaderRightSideComponent } from "./header-right-side/header-right-side.component";
import { HeaderLeftSideComponent } from "./header-left-side/header-left-side.component";
import { HeaderMenuComponent } from "./header-left-side/header-menu/header-menu.component";

@NgModule({
  declarations: [
    HeaderComponent,
    HeaderRightSideComponent,
    HeaderLeftSideComponent,
    HeaderMenuComponent,
  ],
  imports: [
    CommonModule,
    TranslateModule,
    SharedModule
  ],
  exports: [
    HeaderComponent
  ],
})
export class HeaderModule { }
