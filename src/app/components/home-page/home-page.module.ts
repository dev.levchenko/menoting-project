import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TranslateModule } from "@ngx-translate/core";

import { HomePageComponent } from "./home-page.component";
import { SharedModule } from "../../shared/shared.module";

@NgModule({
  declarations: [
    HomePageComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    TranslateModule
  ],
  exports: [
    HomePageComponent
  ]
})
export class HomePageModule { }
