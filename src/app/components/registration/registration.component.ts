import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { FormControl, FormGroup, Validators } from "@angular/forms";

import { v4 as uuidv4 } from "uuid";

import { AuthService } from "../../auth/auth.service";
import { CustomValidators } from "../../utils/customValidators";
import { Registration } from "../../auth/auth.service.api";

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  public form: FormGroup = <FormGroup>{};
  public registrationError: string = '';

  constructor(
    private router: Router,
    private authService: AuthService,
  ) { }

  ngOnInit(): void {
    this.initializeFormGroup();
  }

  initializeFormGroup(): void {
    this.form = new FormGroup({
      userName: new FormControl('', [
        Validators.required,
        CustomValidators.userName
      ]),
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      phoneNumber: new FormControl('', [
        CustomValidators.phoneNumber
      ]),
      birthDate: new FormControl('', [
        CustomValidators.birthDate
      ]),
      password: new FormControl('', [
        Validators.required,
        CustomValidators.password,
      ]),
      passwordConfirmation: new FormControl('', [
        Validators.required,
      ]),
      confirmSendingEmails: new FormControl(false),
      confirmTermsOfUser: new FormControl(false, [
        Validators.requiredTrue
      ]),
    });
  }

  async save(): Promise<void> {
    const isPasswordMatches = this.passwordMatchingValidator();
    if (!isPasswordMatches) return;

    const registration: Registration = {
      id: uuidv4(),
      userName: this.form.value.userName,
      email: this.form.value.email,
      phoneNumber: this.form.value.phoneNumber,
      birthDate: this.form.value.birthDate,
      password: this.form.value.password,
      confirmSendingEmails: this.form.value.confirmSendingEmails
    }

    try {
      await this.authService.registration(registration);
    } catch (error) {
      this.registrationError = error;
      return;
    }

    this.router.navigate(['']);
  }

  isSaveButtonDisabled(): boolean {
    return this.form.invalid
  }

  getValidationErrors(formControlName: string): string[] {
    const formControl = this.form.get(formControlName);
    if (formControl?.errors) {
      return Object.keys(formControl.errors as object);
    }
    return [];
  }

  passwordMatchingValidator(): boolean {
    if (this.form.value.password !== this.form.value.passwordConfirmation) {
      this.form.get('passwordConfirmation')?.setErrors({ passwordsDoesNotMatch: true });
      return false
    }
    return true;
  }

}
