import { Observable } from "rxjs";

export interface Question {

  id: string;
  categoryId: string;
  questionText: string;
  answer: string;

}

export interface Category {

  id: string;
  name: string;
  howManyQuestions: number;
  questions: Question[];
  description?: string;

}

export interface ICategoriesService {

  getCategories(): Promise<Observable<Category[]>>;

  getCategoryById(categoryId: string): Observable<Category | undefined>;

  saveCategory(category: Category): Promise<void>

  updateCategory(categoryId: string, filter: Partial<Category>): Promise<void>

  deleteCategory(categoryId: string): Promise<void>

}
