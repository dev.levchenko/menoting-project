import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from "rxjs";
import { map } from 'rxjs/operators';

import { Category, ICategoriesService } from "./categories.service.api";
import { CategoriesDALService } from "./categoriesDAL.service";

@Injectable({
  providedIn: 'root'
})
export class CategoriesService implements ICategoriesService {

  private categories$ = new BehaviorSubject(<Category[]>[])

  constructor(
    private categoriesDALService: CategoriesDALService
  ) {}

  private async loadCategories(): Promise<void> {
    const categories = await this.categoriesDALService.getCategories();
    this.categories$.next(categories);
  }

  async getCategories(): Promise<Observable<Category[]>> {
    await this.loadCategories();

    return this.categories$.asObservable();
  }

  getCategoryById(categoryId: string): Observable<Category | undefined> {
    return this.categories$.pipe(
      map(categories => categories.find(el => el.id === categoryId)),
    )
  }

  async deleteCategory(categoryId: string): Promise<void> {
    await this.categoriesDALService.deleteCategory(categoryId);

    await this.loadCategories();
  }

  async saveCategory(category: Category): Promise<void> {
    await this.categoriesDALService.saveCategory(category);

    await this.loadCategories();
  }

  async updateCategory(categoryId: string, filter: Partial<Category>): Promise<void> {
    const categoryById = await this.categoriesDALService.getCategoryByID(categoryId);
    if (!categoryById) {
      throw 'categoryChangingFailed';
    }

    if (filter.name) {
      const categoryByName = await this.categoriesDALService.getCategoryByName(filter.name);
      if (categoryByName && categoryByName.id !== categoryById.id) {
        throw 'categoryNameIsBusy';
      }
    }

    const category: Category = {
      id: categoryId,
      name: filter.name || categoryById.name,
      howManyQuestions: categoryById.howManyQuestions,
      description: filter.description || categoryById.description,
      questions: categoryById.questions,
    }

    try {
      await this.categoriesDALService.updateCategory(category);
    } catch (error) {
      throw 'categoryChangingFailed'
    }

    await this.loadCategories();
  }

}
