import { Injectable } from "@angular/core";

import { IDBPDatabase } from "idb";

import { Category } from "./categories.service.api";
import { IndexedDBService } from "../indexedDB/indexedDB.service";

@Injectable({
  providedIn: 'root'
})
export class CategoriesDALService {

  private db = <IDBPDatabase>{};

  constructor(
    private indexedDBService: IndexedDBService
  ) {
    this.indexedDBService.getDBObject().subscribe(db => this.db = db);
  }

  async saveCategory(category: Category): Promise<void> {
    const transaction = this.db.transaction("categories", "readwrite");
    const categories = transaction.objectStore("categories");

    await categories.add(category);
  }

  async getCategories(): Promise<Category[]> {
    const transaction = this.db.transaction("categories", "readonly");
    const categories = transaction.objectStore("categories");

    return await categories.getAll() || <Category[]>[];
  }

  async getCategoryByName(categoryName: string): Promise<Category | undefined> {
    const transaction = this.db.transaction("categories", "readonly");
    const categories = transaction.objectStore("categories");
    const nameIndex = categories.index('name_index');

    return await nameIndex.get(categoryName);
  }

  async getCategoryByID(categoryId: string): Promise<Category | undefined> {
    const transaction = this.db.transaction("categories", "readonly");
    const categories = transaction.objectStore("categories");

    return await categories.get(categoryId);
  }

  async updateCategory(category: Category): Promise<void> {
    const transaction = this.db.transaction("categories", "readwrite");
    const categories = transaction.objectStore("categories");

    await categories.put(category);
  }

  async deleteCategory(categoryId: string): Promise<void> {
    const transaction = this.db.transaction("categories", "readwrite");
    const categories = transaction.objectStore("categories");

    await categories.delete(categoryId);
  }

}
