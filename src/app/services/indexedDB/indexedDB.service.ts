import { Injectable } from "@angular/core";
import { BehaviorSubject, Observable } from "rxjs";
import { take } from "rxjs/operators";

import { IDBPDatabase, openDB } from "idb";

@Injectable({
  providedIn: 'root'
})
export class IndexedDBService {

  public db$ = new BehaviorSubject(<IDBPDatabase>{});

  constructor() {
    this.connectToDB().then(db => this.db$.next(db));
  }

  private async connectToDB(): Promise<IDBPDatabase> {
    return await openDB('ourDB', 1, {
      upgrade(db) {
        const categoriesObjectStore = db.createObjectStore('categories', { keyPath: 'id' });
        categoriesObjectStore.createIndex('name_index', 'name', { unique: true })

        const usersObjectStore = db.createObjectStore('users', { keyPath: 'id' });
        usersObjectStore.createIndex('userName_index', 'userName', { unique: true });
        usersObjectStore.createIndex('email_index', 'email', { unique: true });
      }
    });
  }

  getDBObject(): Observable<IDBPDatabase> {
    return this.db$.pipe(take(2));
  }

}
