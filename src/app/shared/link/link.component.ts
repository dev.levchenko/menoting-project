import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.scss']
})
export class LinkComponent implements OnInit {

  @Input() public name: string = '';
  @Input() public link: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
