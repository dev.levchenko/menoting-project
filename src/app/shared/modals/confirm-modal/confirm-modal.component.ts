import { Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-confirm-modal',
  templateUrl: './confirm-modal.component.html',
  styleUrls: ['./confirm-modal.component.scss']
})
export class ConfirmModalComponent {

  @Input() title: string = '';
  @Input() confirmationMessage: string = '';
  @Input() confirmHandler: Function = <Function>{};
  @Output() onClose: EventEmitter<void> = new EventEmitter<void>();

  constructor() { }

  close(): void {
    this.onClose.emit();
  }

}
