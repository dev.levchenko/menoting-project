import { Component, EventEmitter, OnInit, Output } from '@angular/core';

import { AuthService } from "../../../auth/auth.service";
import { getPasswordHash } from "../../../utils/getPasswordHash";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  @Output() onClose: EventEmitter<void> = new EventEmitter<void>();

  public userName: string = '';
  public password: string = '';
  public error: string = '';

  constructor(
    private authService: AuthService
  ) { }

  ngOnInit(): void {}

  close(): void {
    this.onClose.emit();
  }

  async login(): Promise<void> {
    try {
      const isAuthCorrect = await this.authService.login(this.userName, getPasswordHash(this.password));
      if (isAuthCorrect) {
        this.close();
      }
    } catch (error) {
      this.error = error === 'incorrectAuthDataError' ? error : 'someAuthError';
    }
  }

}
