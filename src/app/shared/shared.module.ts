import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from "@angular/forms";

import { TranslateModule } from "@ngx-translate/core";

import { ButtonComponent } from "./button/button.component";
import { LinkComponent } from "./link/link.component";
import { RouterModule } from "@angular/router";
import { NotFoundComponent } from "./not-found/not-found.component";
import { ConfirmModalComponent } from './modals/confirm-modal/confirm-modal.component';
import { RefDirective } from './ref-directive/ref.directive';
import { LoginComponent } from './modals/login/login.component';

@NgModule({
  declarations: [
    ButtonComponent,
    LinkComponent,
    NotFoundComponent,
    ConfirmModalComponent,
    RefDirective,
    LoginComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    TranslateModule
  ],
  exports: [
    ButtonComponent,
    LinkComponent,
    NotFoundComponent,
    ConfirmModalComponent,
    RefDirective,
    LoginComponent
  ]
})
export class SharedModule { }
