import { createAction } from "@ngrx/store";

export const showLoginModal = createAction('[Modal] show login modal');

export const closeLoginModal = createAction('[Modal] close login modal')

export const showLogoutModal = createAction('[Modal] show logout modal');

export const closeLogoutModal = createAction('[Modal] close logout modal')
