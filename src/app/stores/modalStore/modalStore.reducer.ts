import { createReducer, on } from "@ngrx/store";
import { closeLoginModal, closeLogoutModal, showLoginModal, showLogoutModal } from "./modalStore.actions";

export interface ModalState {
  showLoginModal: boolean;
  showLogoutModal: boolean;
}

export const initialState: ModalState = {
  showLoginModal: false,
  showLogoutModal: false
}

export const modalReducer = createReducer(
  initialState,
  on(showLoginModal, state => ({
    ...state,
    showLoginModal: true
  })),
  on(closeLoginModal, state => ({
    ...state,
    showLoginModal: false,
  })),
  on(showLogoutModal, state => ({
    ...state,
    showLogoutModal: true
  })),
  on(closeLogoutModal, state => ({
    ...state,
    showLogoutModal: false,
  })),
)
