import { createFeatureSelector, createSelector } from "@ngrx/store";

import { ModalState } from "./modalStore.reducer";

export const state = createFeatureSelector<ModalState>('modal');

export const isLoginModal = createSelector(state, state => state.showLoginModal);

export const isLogoutModal = createSelector(state, state => state.showLogoutModal);
