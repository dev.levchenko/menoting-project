import { createAction, props } from "@ngrx/store";

import { Registration } from "../../auth/auth.service.api";

export const setCurrentUser = createAction('[Current User] save current userStore', props<Registration>());

export const clearCurrentUser = createAction('[Current User] clear current userStore');
