import { createReducer, on } from "@ngrx/store";

import { clearCurrentUser, setCurrentUser } from "./user.actions";
import { Registration } from "../../auth/auth.service.api";


export const initialState: Registration = {
  id: '',
  userName: '',
  email: '',
  password: '',
}

export const currentUserReducer = createReducer(
  initialState,
  on(setCurrentUser, (state, { id, userName }) => ({
    ...state,
    id,
    userName
  })),
  on(clearCurrentUser, state => ({
    ...state,
    initialState
  }))
)
