import { createSelector, createFeatureSelector } from "@ngrx/store";

import { Registration } from "../../auth/auth.service.api";

export const state = createFeatureSelector<Registration>('currentUser')

export const getCurrentUser = createSelector(state, user => user);
