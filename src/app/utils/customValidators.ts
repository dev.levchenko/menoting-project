import { AbstractControl, ValidationErrors } from "@angular/forms";

export class CustomValidators {

  static password(formControl: AbstractControl): ValidationErrors | null {
    const passwordRegExp = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$/;
    if (formControl.value && !formControl.value.match(passwordRegExp)) {
      return { incorrectPassword: true }
    }
    return null;
  }

  static phoneNumber(formControl: AbstractControl): ValidationErrors | null {
    const phoneNumberRegExp = /^(\s*)?(\+)?([- _():=+]?\d[- _():=+]?){10,14}(\s*)?$/;
    if (formControl.value && !formControl.value.match(phoneNumberRegExp)) {
      return { invalidPhoneNumber: true }
    }
    return null;
  }

  static userName(formControl: AbstractControl): ValidationErrors | null {
    const userNameRegExp = /^(?=.{6,20}$)(?!.*[_]{3})[\w]+$/;
    if (formControl.value && !formControl.value.match(userNameRegExp)) {
      return { invalidUserName: true }
    }
    return null;
  }

  static birthDate(formControl: AbstractControl): ValidationErrors | null {
    if (new Date(formControl.value) > new Date()) {
      return { invalidBirthDate: true }
    }
    return null;
  }

  static categoryName(formControl: AbstractControl): ValidationErrors | null {
    if (formControl.value.length > 60) {
      return { categoryNameMaxLength: true }
    }

    return null;
  }

}
