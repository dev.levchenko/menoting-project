import { SHA1 } from 'crypto-js'

export function getPasswordHash(password: string): string {
  return SHA1(password).toString();
}
