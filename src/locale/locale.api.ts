import { TranslateLoader, MissingTranslationHandler, MissingTranslationHandlerParams } from "@ngx-translate/core";
import { Observable, of } from "rxjs";

import { localeRu } from "./locale.ru";

export class CustomTranslateLoader implements TranslateLoader {

  getTranslation(lang: string): Observable<object> {
    return of(localeRu);
  }

}

export class MissingTranslationService implements MissingTranslationHandler {

  handle(params: MissingTranslationHandlerParams): string {
    return `WARN: '${params.key}' is missing in '${params.translateService.currentLang}' locale`;
  }

}
